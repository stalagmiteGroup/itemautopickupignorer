# Valheim Item Auto Pickup Ignorer #

### Author: stal4gmite ###

### What is this repository for? ###

* This is a Valheim BepInEx plugin that allows users to disable auto pickup of items.
* Version: 1.0.1

### How do I get set up? ###

* Dependencies:
	* Valheim 0.148.7 (only version tested with)
	* denikson-BepInExPack_Valheim-5.4.901

* Update References:
	* 0Harmony
	* assembly_valheim
	* BepInEx
	* UnityEngine
	* UnityEngine.CoreModule
	* UnityEngine.InputLegacyModule
	* UnityEngine.PhysicsModule

### Plugin Configuration ###
* Copy stal4gmite.ItemAutoPickupIgnorer.cfg to BepInEx\config in your Valheim folder.
* Remove '#' from items you want to ignore.
* In-game, there are 3 modes: Ignoring Items, Ignoring Nothing, and Normal Valheim Behavior. Press left-ctrl + l to cycle through the modes.